const express = require('express');
const app = express();
const port = 3000;

app.set('view engine','pug');
app.set('views','./views');

var users = [
			{id:1,name:'HoangAnh'},
			{id:2,name:"Hoang"}
		];

app.get('/',(req,res)=>{
	res.render('index',{
		name:'AAA'
	});
})

app.get('/user',(req,res)=>{
	res.render('user/index',{
		users:users
	});
})

app.get('/users/search',(req,res)=>{
	var q = req.query.q;
	var matchedUsers = users.filter((user)=>{
		return user.name.toLowerCase().indexOf(q.toLowerCase()) !==-1;
	});
	res.render('user/index',{
		users:matchedUsers
	})
})

app.get("",(req,res)=>{
	res.send("hello");
})

app.listen(port, ()=> console.log('started on port '+port))